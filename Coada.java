import java.util.*;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;

public class Coada extends Thread {
	
	
	private client[] clienti;
	private boolean verificareCasa=false;
	private int idCoada;

   public Coada(int idCoada) {
	super();
	this.clienti = new client[0];
	this.idCoada=idCoada;
   }

   public int getIdCoada()
   {
	   return idCoada;
   }
   
   public void setIdCoada(int idCoada) {
	   this.idCoada=idCoada;
   }
   
   
   
   
   public boolean isVerificareCasa() {
		return verificareCasa;
	}


	public void setVerificareCasa(boolean verificareCasa) {
		this.verificareCasa = verificareCasa;
	}
	
	
   
   
   
   public void addClient(client clientNou)
   {
	   client[] clientiVechi=this.clienti;
	   this.clienti=new client[clientiVechi.length+1];
	   System.arraycopy(clientiVechi, 0, this.clienti, 0, clientiVechi.length);
	   this.clienti[ this.clienti.length-1]=clientNou;
	   
   }

   
  
   
   
   
   
   public void removeClient()
   {
	  
	   client[] clientiVechi=this.clienti;
	   this.clienti=new client[clientiVechi.length-1];
	  System.arraycopy(clientiVechi,1, this.clienti, 0, clientiVechi.length-1);
	   
	   
   }
   
   
  
   
   
   
   public void run() {
	
		while( MainClass.getCurrentTime()<MainClass.getTimeSimulation())
		  {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			};
			
			if(clienti.length>0) {
				client c = clienti[0];  
				int timpProcesare=c.getServiceTime();
				if(timpProcesare==1)
				{
					removeClient();
				}
				else
					{
					
					 timpProcesare=c.getServiceTime()-1;
					   c.setServiceTime(timpProcesare);
					   clienti[0]=c;
					}
					
			  }
		
			}
			
   }
   
   
   
   public int calculareTimpCasa() {
	   
	   int timpCasa=0;
	   
	   for(client client:this.clienti)
	   {
		   timpCasa=timpCasa+client.getServiceTime();
	   }
	   return timpCasa;
   }
   
   
   
   
   
   public String toString() {
		  String s="";
		  if(isVerificareCasa()==false)
			  s= "closed";
		  else{
	               for(client c: clienti){
	                  s= s + c.toString();
	            	   
	               }
	         }
		  return s;	  
	  }
   }
  

    
  

