import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class MainClass {

	 private static int timeArrivalMin;
	 private static int timeArrivalMax;
	 private static int timeServiceMin;
	 private static int timeServiceMax;
	 private static int timeSimulation;
	 private static int currentTime=0;
	// private static int arrivalInterval=(timeArrivalMax - timeArrivalMin) + 1;
	// private static int serviceInterval=(timeServiceMax - timeServiceMin) + 1;
	 public static int nrClienti;
	 public static int nrCozi;
	 public static int idClient=0;
	 public static int timpA;
	 public static int timpA1;
	 public static int timpS;
	
	 private static CopyOnWriteArrayList<client> waitingClients = new CopyOnWriteArrayList<>();

	 private static ArrayList<Coada> cozi = new ArrayList<Coada>();
	 private static ArrayList<Integer> timpArrivalClient = new ArrayList<Integer>();
	 private static String stringSimulare="";



	 public static int getTimeSimulation() {
			return timeSimulation;
		   }
	 
	 
	 public static int getCurrentTime() {
			return currentTime;
		   }

	 
	public static void main(String[] args) {
		
		
		try {
			citesteDateIntrare(args[0]);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		generareClienti();
		//calculareArrivalTimeMinim();
        generareCozi();
        simulare();
        //System.out.println(stringSimulare); 
        scrieDateDeIesire(args[1]);		
	}
	
	

	private static void citesteDateIntrare(String numeIntrare) throws IOException{
		
     	String citeste;
		FileInputStream f=new FileInputStream(numeIntrare);
		InputStreamReader fchar=new InputStreamReader(f);
		BufferedReader buf=new BufferedReader(fchar);
		citeste=buf.readLine();
		nrClienti=Integer.parseInt(citeste);
		//System.out.println(nrClienti);
		
		citeste=buf.readLine();
		nrCozi=Integer.parseInt(citeste);
		//System.out.println(nrCozi);
		
		citeste=buf.readLine();
		timeSimulation=Integer.parseInt(citeste);
		citeste=buf.readLine();
		String[] tArr = citeste.split(",");
		
		timeArrivalMin=Integer.parseInt(tArr[0]);
		timeArrivalMax=Integer.parseInt(tArr[1]);
		//System.out.println(timeArrivalMin); 
		//System.out.println(timeArrivalMax); 
		
		 citeste=buf.readLine();
		 String[] tServ = citeste.split(",");

			
		 timeServiceMin=Integer.parseInt(tServ[0]);
		 timeServiceMax=Integer.parseInt(tServ[1]);
		
		}
		
	
	public static  void scrieDateDeIesire(String numeIesire) {
		
		    try {
		      FileWriter myWriter = new FileWriter(numeIesire);
		      myWriter.write(stringSimulare);
		      myWriter.close();
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		  }
		
	
	
	private static void generareClienti() {
		
		
		 for(int i=1;i<=nrClienti;i++)
		   {
			    timpA = (int) (Math.random()*(timeArrivalMax - timeArrivalMin) + 1)+timeArrivalMin;
			    
			    if(timpArrivalClient.isEmpty())
			    {
			    	timpArrivalClient.add(timpA);
			    }
			    else
			    	
			    {
			    	while(timpArrivalClient.contains(timpA))
			    	{
			    		
			    		timpA = (int) (Math.random()*(timeArrivalMax - timeArrivalMin) + 1)+timeArrivalMin;
			    		
			    	}
			    	timpArrivalClient.add(timpA);
			    }
			    
			    
		   
			    timpS = (int) (Math.random()*(timeServiceMax - timeServiceMin) + 1)+timeServiceMin;
			    client c=new client(i,timpA,timpS);
			   
			    	 waitingClients.add(c);
			       
			   
					
		   }
		
		
		
	}
	
	
		
	  private static void generareCozi() {
			
		        for(int i=0;i<nrCozi;i++)
		        {
			   Coada coada = new Coada(i+1);
			   cozi.add(coada);  
			   //System.out.println(cozi.toString());
		        }
				
		   }
	
	

	 	  
	  

	     public static client calculareArrivalTimeMinim() {
	    	 
	    	 client minArrivalClient = waitingClients.get(0);
			  for(client cl:waitingClients) {
				  if(cl. getArrivalTime()<minArrivalClient.getArrivalTime())
				  {
					  minArrivalClient=cl;
				  }
				
			
			  }
			return minArrivalClient;
			
		  }
	     
	     
	     
	  
     
	     public static Coada getCoadaInchisa() {
	 		
	 		for(Coada coada:cozi)
	 		{
	 			if(coada.isVerificareCasa()==false)  
	 				
	 				return coada;
	 			
	 		}
	 		return null;
	 		
	 	}
	     
	     
	     
	     private static Coada gasesteCasaCuTimpMinim(){
		 	   
		    	
	    	 Coada minServiceCoada = cozi.get(0);

	    	 for(Coada coada : cozi)
	    	 {
	    		if(minServiceCoada.calculareTimpCasa() < coada.calculareTimpCasa())
	    		{
	    			minServiceCoada=coada;
	    		}
	    			
	    		
	    	 }
	    	 return minServiceCoada;
	 
	 	   

	 	}
	     
	     
	     
	     public static void simulare() {
	    	 while(currentTime<timeSimulation) {
				 currentTime++;

				 if (!waitingClients.isEmpty()) {
					 client c = calculareArrivalTimeMinim();

					 if (currentTime == c.getArrivalTime()) {
						 Coada coadaInchisa = getCoadaInchisa();


						 if (coadaInchisa != null) {
							 coadaInchisa.setVerificareCasa(true);
							 coadaInchisa.addClient(c);
							 waitingClients.remove(c);
							 coadaInchisa.start();
						 } else {
							 Coada coadaTimpMinim = gasesteCasaCuTimpMinim();
							 coadaTimpMinim.addClient(c);
							 waitingClients.remove(c);
						 }


					 }
				 }

				 stringSimulare += "\nTime:" + currentTime + "\n" + "Waiting clients: " + waitingClients.toString();


				 for (Coada coada : cozi) {
					 stringSimulare += "\n Queue " + coada.getIdCoada() + ": " + coada.toString();

				 }
				 //System.out.println(stringSimulare);
				 try {
					 Thread.sleep(10);
				 } catch (InterruptedException e) {
					 e.printStackTrace();
				 }
	    	 }
	    	 
	    	 
	     }
	  
	  
	
}
