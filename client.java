
public class client {
   private int id;
   private int arrivalTime;
   private int serviceTime;
   private int finishTime;
   

   
   public client(int id, int arrivalTime, int serviceTime) {
	super();
	this.id = id;
	this.arrivalTime = arrivalTime;
	this.serviceTime = serviceTime;
	this.finishTime = 0;
   }

public int getId() {
	return id;
   }
   
   public void setId(int id) {
	this.id = id;
   }
   
   public int getArrivalTime() {
	return arrivalTime;
   }
   
   public void setArrivalTime(int arrivalTime) {
	this.arrivalTime = arrivalTime;
   }
   
   
   
   public int getServiceTime() {
		return serviceTime;
	   }
	   
   
   public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	   }
   
   
   
   public int getFinishTime() {
	return finishTime;
   }
   
   public void setFinishTime(int finishTime) {
	this.finishTime = finishTime;
   }
   
   
   
  
   
   
   public String toString() {
	   String s;
	   s=( "(" +  getId() + "," + getArrivalTime() + "," + getServiceTime() + ")" );
	   return s;
   }
   
}
